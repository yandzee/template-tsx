const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	mode: 'development',
	entry: './src/index.tsx',
	output: {
		filename: 'index.js',
		path: path.resolve(__dirname, './dist'),
	},
	resolve: {
	    extensions: [".ts", ".tsx", ".js", ".jsx"]
	},
	devtool: "source-map",
	devServer: {
		contentBase: "./dist",
	},
	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				exclude: /node_modules/,
				use: ['babel-loader'],
			},
			{
				test: /\.js$/,
				use: ["source-map-loader"],
				exclude: /node_modules/,
				enforce: "pre"
			},
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: 'src/index.html',
		}),
	]
};
